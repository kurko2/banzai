#ifndef FGURELISTMODEL_H
#define FGURELISTMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QList>
#include <QHash>
#include "AbstractRenderNode.h"



class FigureListModel : public QAbstractListModel
{
    Q_OBJECT

public:
    FigureListModel();
    ~FigureListModel();

    void append(AbstractRenderNode *node);
    void remove(const quint32 uniqueId);
    QSGNode *getNode(const quint32 uniqueId);


    // Обязаны быть переопределены
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QHash<int, QByteArray> roleNames() const;

private:
    enum Column {
        TYPE_ROLE,
        UNIQUE_ID_ROLE
    };

    QList<AbstractRenderNode *> m_listFigures;
    QHash<quint32, AbstractRenderNode *> m_hashFigures;

    quint32 m_uniqueIdCounter;
};

#endif // FGURELISTMODEL_H
