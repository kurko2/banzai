#ifndef TRIANGLERENDERNODE_H
#define TRIANGLERENDERNODE_H

#include <QObject>
#include <qsgrendernode.h>
#include "AbstractRenderNode.h"

QT_BEGIN_NAMESPACE

class QQuickItem;
class QOpenGLShaderProgram;
class QOpenGLBuffer;

QT_END_NAMESPACE

class TriangleRenderNode : public AbstractRenderNode
{
public:
    TriangleRenderNode(QQuickItem *item);
    ~TriangleRenderNode();

    void render(const RenderState *state) override;
    void releaseResources() override;
    StateFlags changedStates() const override;
    RenderingFlags flags() const override;
    QRectF rect() const override;

    QString getType() const;
    void doSelect(bool select);

private:   
    void init();
    QQuickItem *m_item;

    const int vertexSize;

    QOpenGLShaderProgram *m_program;
    int m_matrixUniform;
    int m_opacityUniform;
    QOpenGLBuffer *m_vbo;

    QPointF p0;
    QPointF p1;
    QPointF p2;


};

#endif // TRIANGLERENDERNODE_H
