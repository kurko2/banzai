#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "FigureListModel.h"
#include "FigureItem.h"

FigureListModel * figureListModel = nullptr;

int main(int argc, char *argv[])
{
    //QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);

    qmlRegisterType<FigureItem>("FigureView", 1, 0, "FigureItem");

    QQmlApplicationEngine engine;

    figureListModel = new FigureListModel();

    engine.rootContext()->setContextProperty("figureListModel", figureListModel);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
