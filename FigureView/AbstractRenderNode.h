#ifndef ABSTRACTRENDERNODE_H
#define ABSTRACTRENDERNODE_H

#include <QObject>
#include <qsgrendernode.h>


class AbstractRenderNode : public QSGRenderNode
{
public:
    AbstractRenderNode();
    ~AbstractRenderNode();

    virtual QString getType() const = 0;
    virtual void doSelect(bool select) = 0;


private:
    //QString type;
};

#endif // ABSTRACTRENDERNODE_H
