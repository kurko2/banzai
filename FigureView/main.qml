import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import FigureView 1.0

Window {
    visible: true
    width: 1024
    height: 600
    title: qsTr("Визуализатор геометрических фигур")


    ColumnLayout {
        anchors.fill: parent
        Layout.fillHeight: true


        RowLayout {
            Layout.fillWidth: true
            spacing: 10

            Button {
                Layout.fillWidth: true
                text: qsTr("Окружность")
                onClicked: {
                    rendererFigure.createCircle()
                }
            }
            Button {
                Layout.fillWidth: true
                text: qsTr("Квадрат")
                onClicked: {
                    rendererFigure.createSquare()
                }
            }
            Button {
                Layout.fillWidth: true
                text: qsTr("Треугольник")
                onClicked: {
                    rendererFigure.createTriangle()
                }
            }
        }

        RowLayout {
            Rectangle {
                Layout.margins: 5
                Layout.fillWidth: true
                Layout.fillHeight: true
                color: "gray"

                FigureItem {
                    id: rendererFigure
                    anchors.fill: parent
                    anchors.margins: 10
                }
            }

            ListView {
                id: listView
                Layout.margins: 5
                Layout.fillHeight: true
                width: 300
                height: parent.height
                highlight: Rectangle {  opacity: 0.5; color: "lightsteelblue"; radius: 5 }
                spacing: 10
                model: figureListModel
                delegate: OutlinerItem {  }
            }
        }
    }
}
