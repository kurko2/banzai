#include "FigureItem.h"
#include "CircleRenderNode.h"
#include "TriangleRenderNode.h"
#include "SquareRenderNode.h"
#include <QQuickWindow>
#include <QSGRendererInterface>

extern FigureListModel * figureListModel;

FigureItem::FigureItem(QQuickItem *parent):
    QQuickItem(parent),
    rootNode(nullptr)
{
    rootNode = new QSGNode();
    setFlag(ItemHasContents);    
}

QSGNode *FigureItem::updatePaintNode(QSGNode *node, QQuickItem::UpdatePaintNodeData *)
{    
    Q_UNUSED(node)
    return rootNode;
}

void FigureItem::createCircle()
{
    AbstractRenderNode * node = new CircleRenderNode(this);
    rootNode->appendChildNode(node);
    figureListModel->append(node);
}

void FigureItem::createTriangle()
{
    AbstractRenderNode * node = new TriangleRenderNode(this);
    rootNode->appendChildNode(node);
    figureListModel->append(node);
}

void FigureItem::createSquare()
{
    AbstractRenderNode * node = new SquareRenderNode(this);
    rootNode->appendChildNode(node);
    figureListModel->append(node);
}

void FigureItem::removeFigure(const quint32 uniqueId)
{
    QSGNode * n = figureListModel->getNode(uniqueId);
    rootNode->removeChildNode(n);
    figureListModel->remove(uniqueId);
}

void FigureItem::doSelect(const quint32 uniqueId, const bool select)
{
    AbstractRenderNode * n = (AbstractRenderNode * )figureListModel->getNode(uniqueId);
    n->doSelect(select);
}
