#include "SquareRenderNode.h"

#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLFunctions>
#include <QQuickItem>
#include <QRandomGenerator>


SquareRenderNode::SquareRenderNode(QQuickItem *item):
    m_item(item),
    vertexSize(VERTEX_NUMBER_SQUARE/2 * sizeof(GLfloat)),
    m_program(nullptr),
    m_vbo(nullptr),
    p0(QRandomGenerator::global()->bounded(m_item->width()-50), QRandomGenerator::global()->bounded(m_item->height()-50)),
    p1(p0.x()+50, p0.y()+50),
    p2(p1.x(), p0.y()),
    p3(p0.x(), p1.y()),
    lineWidth(1)

{
}

SquareRenderNode::~SquareRenderNode()
{
    if (m_program) {
        delete m_program;
        m_program = nullptr;
    }
    if (m_vbo) {
        delete m_vbo;
        m_vbo = nullptr;
    }
}

void SquareRenderNode::render(const QSGRenderNode::RenderState *state)
{
    if (!m_program)
        init();

    QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();

    m_program->bind();
    m_program->setUniformValue(m_matrixUniform, *state->projectionMatrix() * *matrix());
    m_program->setUniformValue(m_opacityUniform, float(inheritedOpacity()));

    m_vbo->bind();

    GLfloat vertices[VERTEX_NUMBER_SQUARE] = {
        GLfloat(p0.x()), GLfloat(p0.y()),
        GLfloat(p2.x()), GLfloat(p2.y()),
        GLfloat(p1.x()), GLfloat(p1.y()),
        GLfloat(p3.x()), GLfloat(p3.y())
    };

    m_vbo->write(0, vertices, sizeof(vertices));

    m_program->setAttributeBuffer(0, GL_FLOAT, 0, 2);
    m_program->setAttributeBuffer(1, GL_FLOAT, sizeof(vertices), 3);
    m_program->enableAttributeArray(0);
    m_program->enableAttributeArray(1);

    f->glEnable(GL_BLEND);
    f->glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    //f->glDrawArrays(GL_QUADS, 0, VERTEX_NUMBER/2);
    f->glLineWidth(lineWidth);
    f->glDrawArrays(GL_LINE_LOOP, 0, VERTEX_NUMBER_SQUARE/2);
}

void SquareRenderNode::releaseResources()
{
    if (m_program) {
        delete m_program;
        m_program = nullptr;
    }
    if (m_vbo) {
        delete m_vbo;
        m_vbo = nullptr;
    }
}

QSGRenderNode::StateFlags SquareRenderNode::changedStates() const
{
    return BlendState;
}

QSGRenderNode::RenderingFlags SquareRenderNode::flags() const
{
    return BoundedRectRendering | DepthAwareRendering;
}

QRectF SquareRenderNode::rect() const
{
    return QRect(0, 0, m_item->width(), m_item->height());
}

QString SquareRenderNode::getType() const
{
    return QString(QObject::tr("Квадрат"));
}

void SquareRenderNode::doSelect(bool select)
{
    (select) ? (lineWidth = 5) : (lineWidth = 1);
}

void SquareRenderNode::init()
{
    static GLfloat colors[] = {
        1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 1.0f
    };
    static const char *vertexShaderSource =
        "attribute highp vec4 posAttr;\n"
        "attribute lowp vec4 colAttr;\n"
        "varying lowp vec4 col;\n"
        "uniform highp mat4 matrix;\n"
        "void main() {\n"
        "   col = colAttr;\n"
        "   gl_Position = matrix * posAttr;\n"
        "}\n";

    static const char *fragmentShaderSource =
        "varying lowp vec4 col;\n"
        "uniform lowp float opacity;\n"
        "void main() {\n"
        "   gl_FragColor = col * opacity;\n"
        "}\n";

    m_program = new QOpenGLShaderProgram;

    m_program->addCacheableShaderFromSourceCode(QOpenGLShader::Vertex, vertexShaderSource);
    m_program->addCacheableShaderFromSourceCode(QOpenGLShader::Fragment, fragmentShaderSource);
    m_program->bindAttributeLocation("posAttr", 0);
    m_program->bindAttributeLocation("colAttr", 1);
    m_program->link();

    m_matrixUniform = m_program->uniformLocation("matrix");
    m_opacityUniform = m_program->uniformLocation("opacity");

    m_vbo = new QOpenGLBuffer;
    m_vbo->create();
    m_vbo->bind();
    m_vbo->allocate(vertexSize + sizeof(colors));
    m_vbo->write(vertexSize, colors, sizeof(colors));
    m_vbo->release();
}
