#ifndef FIGUREITEM_H
#define FIGUREITEM_H

#include <QObject>
#include <QQuickItem>
#include "FigureListModel.h"

class FigureItem : public QQuickItem
{
    Q_OBJECT

public:
    FigureItem(QQuickItem *parent = nullptr);
    QSGNode *updatePaintNode(QSGNode *node, UpdatePaintNodeData *) override;

    quint32 getUniqueId()
    { return uniqueId; }

    Q_INVOKABLE void createCircle();
    Q_INVOKABLE void createTriangle();
    Q_INVOKABLE void createSquare();
    Q_INVOKABLE void removeFigure(const quint32 uniqueId);

    Q_INVOKABLE void doSelect(const quint32 uniqueId, const bool select);



private:
    quint32 uniqueId;
    QSGNode * rootNode;
};

#endif // FIGUREITEM_H
