#ifndef CIRCLERENDERNODE_H
#define CIRCLERENDERNODE_H

#include <QObject>
#include <qsgrendernode.h>
#include "AbstractRenderNode.h"

QT_BEGIN_NAMESPACE

class QQuickItem;
class QOpenGLShaderProgram;
class QOpenGLBuffer;

QT_END_NAMESPACE

#define VERTEX_NUMBER   720

class CircleRenderNode : public AbstractRenderNode
{
public:
    CircleRenderNode(QQuickItem *item);
    ~CircleRenderNode();

    void render(const RenderState *state) override;
    void releaseResources() override;
    StateFlags changedStates() const override;
    RenderingFlags flags() const override;
    QRectF rect() const override;

    QString getType() const;
    void doSelect(bool select);

private:   
    void init();
    QQuickItem *m_item;

    const int vertexSize;

    QOpenGLShaderProgram *m_program;
    int m_matrixUniform;
    int m_opacityUniform;
    QOpenGLBuffer *m_vbo;

    float centerX;
    float centerY;

    GLfloat vertices[VERTEX_NUMBER];

    bool changeColor;
};

#endif // CIRCLERENDERNODE_H
