#ifndef SQUARERENDERNODE_H
#define SQUARERENDERNODE_H

#include <QObject>
#include <qsgrendernode.h>
#include "AbstractRenderNode.h"

QT_BEGIN_NAMESPACE

class QQuickItem;
class QOpenGLShaderProgram;
class QOpenGLBuffer;

QT_END_NAMESPACE

#define VERTEX_NUMBER_SQUARE   8

class SquareRenderNode : public AbstractRenderNode
{
public:
    SquareRenderNode(QQuickItem *item);
    ~SquareRenderNode();

    void render(const RenderState *state) override;
    void releaseResources() override;
    StateFlags changedStates() const override;
    RenderingFlags flags() const override;
    QRectF rect() const override;

    QString getType() const;
    void doSelect(bool select);

private:   
    void init();
    QQuickItem *m_item;

    const int vertexSize;

    QOpenGLShaderProgram *m_program;
    int m_matrixUniform;
    int m_opacityUniform;
    QOpenGLBuffer *m_vbo;

    QPointF p0;
    QPointF p1;
    QPointF p2;
    QPointF p3;

    quint8 lineWidth;


};

#endif // SQUARERENDERNODE_H
