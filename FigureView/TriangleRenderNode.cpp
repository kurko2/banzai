#include "TriangleRenderNode.h"

#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLFunctions>
#include <QQuickItem>
#include <QRandomGenerator>



TriangleRenderNode::TriangleRenderNode(QQuickItem *item):
    m_item(item),
    vertexSize(6 * sizeof(GLfloat)),
    m_program(nullptr),
    m_vbo(nullptr),
    p0(QRandomGenerator::global()->bounded(m_item->width()-50), QRandomGenerator::global()->bounded(m_item->height()-50)),
    p1(p0.x()+41, p0.y()+25),
    p2(p0.x()+41, p0.y()-25)

{
}

TriangleRenderNode::~TriangleRenderNode()
{
    releaseResources();
}

void TriangleRenderNode::render(const QSGRenderNode::RenderState *state)
{
    if (!m_program)
        init();

    QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();

    m_program->bind();
    m_program->setUniformValue(m_matrixUniform, *state->projectionMatrix() * *matrix());
    m_program->setUniformValue(m_opacityUniform, float(inheritedOpacity()));

    m_vbo->bind();

    GLfloat vertices[6] = {
        GLfloat(p0.x()), GLfloat(p0.y()),
        GLfloat(p1.x()), GLfloat(p1.y()),
        GLfloat(p2.x()), GLfloat(p2.y())
    };

    m_vbo->write(0, vertices, sizeof(vertices));

    m_program->setAttributeBuffer(0, GL_FLOAT, 0, 2);
    m_program->setAttributeBuffer(1, GL_FLOAT, sizeof(vertices), 3);
    m_program->enableAttributeArray(0);    
    m_program->enableAttributeArray(1);

    f->glEnable(GL_BLEND);
    f->glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    f->glLineWidth(1);
    f->glDrawArrays(GL_LINE_LOOP, 0, 3);
}

void TriangleRenderNode::releaseResources()
{
    if (m_program) {
        delete m_program;
        m_program = nullptr;
    }
    if (m_vbo) {
        delete m_vbo;
        m_vbo = nullptr;
    }
}

QSGRenderNode::StateFlags TriangleRenderNode::changedStates() const
{
    return BlendState;
}

QSGRenderNode::RenderingFlags TriangleRenderNode::flags() const
{
    return BoundedRectRendering | DepthAwareRendering;
}

QRectF TriangleRenderNode::rect() const
{
    return QRect(0, 0, m_item->width(), m_item->height());
}

QString TriangleRenderNode::getType() const
{
    return QString(QObject::tr("Треугольник"));
}

void TriangleRenderNode::doSelect(bool select)
{
    if (select) {
        p0.setX(p0.x()-9);
        p1.setX(p1.x()+9); p1.setY(p1.y()+7);
        p2.setX(p2.x()+9); p2.setY(p2.y()-7);
    } else {
        p0.setX(p0.x()+9);
        p1.setX(p1.x()-9); p1.setY(p1.y()-7);
        p2.setX(p2.x()-9); p2.setY(p2.y()+7);
    }

}

void TriangleRenderNode::init()
{
    static GLfloat colors[] = {
        1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 1.0f
    };
    static const char *vertexShaderSource =
        "attribute highp vec4 posAttr;\n"
        "attribute lowp vec4 colAttr;\n"
        "varying lowp vec4 col;\n"
        "uniform highp mat4 matrix;\n"
        "void main() {\n"
        "   col = colAttr;\n"
        "   gl_Position = matrix * posAttr;\n"
        "}\n";

    static const char *fragmentShaderSource =
        "varying lowp vec4 col;\n"
        "uniform lowp float opacity;\n"
        "void main() {\n"
        "   gl_FragColor = col * opacity;\n"
        "}\n";

    m_program = new QOpenGLShaderProgram;

    m_program->addCacheableShaderFromSourceCode(QOpenGLShader::Vertex, vertexShaderSource);
    m_program->addCacheableShaderFromSourceCode(QOpenGLShader::Fragment, fragmentShaderSource);
    m_program->bindAttributeLocation("posAttr", 0);
    m_program->bindAttributeLocation("colAttr", 1);
    m_program->link();

    m_matrixUniform = m_program->uniformLocation("matrix");
    m_opacityUniform = m_program->uniformLocation("opacity");

    m_vbo = new QOpenGLBuffer;
    m_vbo->create();
    m_vbo->bind();
    m_vbo->allocate(vertexSize + sizeof(colors));
    m_vbo->write(vertexSize, colors, sizeof(colors));
    m_vbo->release();
}
