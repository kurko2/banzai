#include <QVariant>
#include "FigureListModel.h"

FigureListModel::FigureListModel():
    m_uniqueIdCounter(10)
{

}

FigureListModel::~FigureListModel()
{
    for (auto figure : m_listFigures)
        delete figure;

    m_listFigures.clear();
}

void FigureListModel::append(AbstractRenderNode * node)
{
    beginInsertRows(QModelIndex(), m_listFigures.size(), m_listFigures.size());
    m_listFigures.append(node);
    m_hashFigures.insert(m_uniqueIdCounter++, node);
    endInsertRows();
}

void FigureListModel::remove(const quint32 uniqueId)
{
    QHash<quint32, AbstractRenderNode *>::const_iterator iter = m_hashFigures.constFind(uniqueId);
    if (iter == m_hashFigures.constEnd())
        return;

    qint32 index = m_listFigures.indexOf(iter.value());

    beginRemoveRows(QModelIndex(), index, index);
    m_listFigures.removeAt(index);
    m_hashFigures.take(iter.key());
    endRemoveRows();
}

QSGNode *FigureListModel::getNode(const quint32 uniqueId)
{
    QHash<quint32, AbstractRenderNode *>::const_iterator iter = m_hashFigures.constFind(uniqueId);
    if (iter != m_hashFigures.constEnd())
        return iter.value();

    return nullptr;
}

int FigureListModel::rowCount(const QModelIndex &parent) const
{
    return m_listFigures.size();
}

QVariant FigureListModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid() || (index.row()>=m_listFigures.size()))
        return QVariant();

    switch (role)
    {
    case TYPE_ROLE:
        return m_listFigures.at(index.row())->getType();
        break;
    case UNIQUE_ID_ROLE:
        return m_hashFigures.key( m_listFigures.at(index.row()) );
        break;
    default:
        return QVariant();
    }
}

QHash<int, QByteArray> FigureListModel::roleNames() const
{
    const QHash<int, QByteArray> roles({{TYPE_ROLE, "figureType"}, {UNIQUE_ID_ROLE, "uniqueId"}});
    return roles;
}
