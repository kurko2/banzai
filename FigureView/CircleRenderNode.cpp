#include "CircleRenderNode.h"

#include <QOpenGLShaderProgram>
#include <QOpenGLBuffer>
#include <QOpenGLFunctions>
#include <QQuickItem>
#include <QRandomGenerator>



CircleRenderNode::CircleRenderNode(QQuickItem *item):
    m_item(item),
    vertexSize(VERTEX_NUMBER * sizeof(GLfloat)),
    m_program(nullptr),
    m_vbo(nullptr),
    centerX(50+QRandomGenerator::global()->bounded(m_item->width()-50)),
    centerY(50+QRandomGenerator::global()->bounded(m_item->height()-50)),
    changeColor(false)

{
    for (int ii = 0; ii < VERTEX_NUMBER; ii+=2)   {
        float theta = 2.0f * 3.1415926f * float(ii/2) / float(VERTEX_NUMBER/2);
        float x = 25 * cosf(theta);
        float y = 25 * sinf(theta);
        QPointF p(centerX + x , centerY + y);
        vertices[ii] = p.x();
        vertices[ii+1] = p.y();
    }

}

CircleRenderNode::~CircleRenderNode()
{
    releaseResources();
}

void CircleRenderNode::render(const QSGRenderNode::RenderState *state)
{
    if (!m_program)
        init();

    QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();

    m_program->bind();
    m_program->setUniformValue(m_matrixUniform, *state->projectionMatrix() * *matrix());
    m_program->setUniformValue(m_opacityUniform, float(inheritedOpacity()));

    m_vbo->bind();

    m_vbo->write(0, vertices, sizeof(vertices));

    m_program->setAttributeBuffer(0, GL_FLOAT, 0, 2);
    m_program->setAttributeBuffer(1, GL_FLOAT, sizeof(vertices), 3);
    m_program->enableAttributeArray(0);
    m_program->enableAttributeArray(1);

    f->glEnable(GL_BLEND);
    f->glBlendFunc(GL_ONE, GL_ONE_MINUS_SRC_ALPHA);
    if (changeColor)
        f->glColorMask(GLboolean(false), GLboolean(true), GLboolean(false), GLboolean(false));
    f->glLineWidth(1);
    f->glDrawArrays(GL_LINE_LOOP, 0, VERTEX_NUMBER/2);
    if (changeColor)
        f->glColorMask(GLboolean(true), GLboolean(true), GLboolean(true), GLboolean(true));
}

void CircleRenderNode::releaseResources()
{
    if (m_program) {
        delete m_program;
        m_program = nullptr;
    }
    if (m_vbo) {
        delete m_vbo;
        m_vbo = nullptr;
    }
}

QSGRenderNode::StateFlags CircleRenderNode::changedStates() const
{
    return BlendState;
}

QSGRenderNode::RenderingFlags CircleRenderNode::flags() const
{
    return BoundedRectRendering | DepthAwareRendering;
}

QRectF CircleRenderNode::rect() const
{
    return QRect(0, 0, m_item->width(), m_item->height());
}

QString CircleRenderNode::getType() const
{
    return QString(QObject::tr("Круг"));
}

void CircleRenderNode::doSelect(bool select)
{
    changeColor = select;
}

void CircleRenderNode::init()
{
    static GLfloat colors[] = {
        1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 1.0f
    };
    static const char *vertexShaderSource =
        "attribute highp vec4 posAttr;\n"
        "attribute lowp vec4 colAttr;\n"
        "varying lowp vec4 col;\n"
        "uniform highp mat4 matrix;\n"
        "void main() {\n"
        "   col = colAttr;\n"
        "   gl_Position = matrix * posAttr;\n"
        "}\n";

    static const char *fragmentShaderSource =
        "varying lowp vec4 col;\n"
        "uniform lowp float opacity;\n"
        "void main() {\n"
        "   gl_FragColor = col * opacity;\n"
        "}\n";

    m_program = new QOpenGLShaderProgram;

    m_program->addCacheableShaderFromSourceCode(QOpenGLShader::Vertex, vertexShaderSource);
    m_program->addCacheableShaderFromSourceCode(QOpenGLShader::Fragment, fragmentShaderSource);
    m_program->bindAttributeLocation("posAttr", 0);
    m_program->bindAttributeLocation("colAttr", 1);
    m_program->link();

    m_matrixUniform = m_program->uniformLocation("matrix");
    m_opacityUniform = m_program->uniformLocation("opacity");

    m_vbo = new QOpenGLBuffer;
    m_vbo->create();
    m_vbo->bind();
    m_vbo->allocate(vertexSize + sizeof(colors));
    m_vbo->write(vertexSize, colors, sizeof(colors));
    m_vbo->release();
}
