import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

RowLayout {
    id: root
    width: parent.width
    height: button.height
    Button {
        id: button
        text: qsTr("Удалить")
        onClicked: {
            rendererFigure.removeFigure(uniqueId)
        }
    }

    Rectangle {
        Layout.fillWidth: true
        height: button.height
        CheckBox {
            id: checkBox
            text: qsTr("%1 %2").arg(figureType).arg(uniqueId)
            height: button.height
            onCheckedChanged: {
                (rendererFigure.doSelect(uniqueId, checked))

            }
        }
        MouseArea {
            anchors.fill: parent
            onClicked: {
                checkBox.checked = !checkBox.checked
                listView.currentIndex = index
            }
        }
    }
}
